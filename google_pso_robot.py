""" Python 3.5
Takes a list of urls and retrieves the Google Page Speed Scores for Mobile and Desktop
"""
# O    TODO [version 0.2]: Put the api key in an untracked file for git ignore
# O    TODO [version 0.2]: Enable ARGV ProjectName => make dir in projects if not exists where all data is stored
import sys
import requests
import json
from lxml import etree, html

from google_api_key import GOOGLE_API_KEY

# Your Page Speed API Key | Get it here: https://console.developers.google.com/
google_api_key = GOOGLE_API_KEY
# File Path to the list of URLs (one per line)
urls_filepath = 'url_list.txt'
# File Path where the reports will be stored
json_report_path = 'report.json'
html_report_path = 'report.html'

# Report of the Speed Scores
report_dict = {}

#print(ARGV)

#exit()

def load_urls(filepath):
    """ Loads all urls from file """
    f = open(filepath, 'r')
    urls = f.read().splitlines()
    f.close()
    return urls



def run_google_pagespeed(url):
    # O    TODO [version 0.2]: Test if result is returned or API Call was bad
    # Returns dict {desktop:,mobile:}
    BASE_REQUEST = "https://www.googleapis.com/pagespeedonline/v4/runPagespeed?url=" + url + "&key=" + google_api_key
    desktopPSO_Request = BASE_REQUEST + "&strategy=desktop"
    mobilePSO_Request = BASE_REQUEST + "&strategy=mobile"
    # Fetch the Results
    desktopPSO_Result = requests.get( desktopPSO_Request )
    mobilePSO_Result = requests.get( mobilePSO_Request )
    return {'desktop':desktopPSO_Result.json(), 'mobile':mobilePSO_Result.json()}



def generate_report_html(report_dict):
    """ Generates HTML Google PageSpeed Report """
    # Initializing HTML Sting
    html_string = "<html><head></head><body>"
    # Heading
    html_string += "<h1>Google Page Speed Report</h1>"
    #
    # Generate Overview
    #
    html_string += "<h2>Overview</h2>"
    html_string += "<table><tr><th>URL</th><th>Desktop Score</th><th>Mobile Score</th></tr>"
    for url, page_dict in report_dict.items():
        html_string += "<tr><td>{url}</td><td>{desktop_score} %</td><td>{mobile_score} %</td></tr>".format(
            url = url,
            desktop_score = page_dict['desktop']['ruleGroups']['SPEED']['score'],
            mobile_score = page_dict['mobile']['ruleGroups']['SPEED']['score']
        )
    html_string += "</table>"
    #
    # Generate Detail Report
    #
    html_string += "<h2>Details</h2>"
    for url, page_dict in report_dict.items():
        html_string += "<h3>{}</h3><p>".format( url )
        html_string += json.dumps(page_dict, ensure_ascii=False)
        html_string += "</p>"
    # Closing and Returning Report
    html_string += "</body></html>"
    return html_string



def write_html_report(filepath, html_string):
    """ PrettyPrints the HTM and write it to file """
    document_root = html.fromstring(html_string)
    pretty_html = etree.tostring(document_root, encoding='unicode', pretty_print=True)
    # Write to file
    f = open(filepath, 'w')
    f.write(pretty_html)
    f.close()



def write_json_report(filepath, report_dict):
    json_report = json.dumps(report_dict, ensure_ascii=False)
    f = open(filepath, 'w')
    f.write(json_report)
    f.close()



print("Loading URL's and Running Google PageSpeed API...")
url_array = load_urls(urls_filepath)

for url in url_array:
    print('Retrieving PageSpeed Report for: %s' % url )
    report = run_google_pagespeed(url)
    print('Desktop: %s | Mobile: %s' % (report['desktop']['ruleGroups']['SPEED']['score'], report['mobile']['ruleGroups']['SPEED']['score']) )
    # Add Reports to the URL
    report_dict[url] = report

print("Generating Google PageSpeed Report...")
# JSON REPORT
write_json_report(json_report_path, report_dict)
# HTML REPORT
report_html_string = generate_report_html(report_dict)
write_html_report(html_report_path, report_html_string)

print("Done!")
